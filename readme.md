# Backbone Portfolio

This is a stripped-down version of the portfolio used on [KentHeberling.com](http://web.kentheberling.com). Category filtering is removed, but portfolio editing using local storeage has been added. Go ahead and play around!