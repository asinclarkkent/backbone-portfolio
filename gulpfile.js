var cache       = require('gulp-cached');
var concat      = require('gulp-concat');
var csscomb     = require('gulp-csscomb');
var cssnano     = require('gulp-cssnano');
var gulp        = require('gulp');
var header      = require('gulp-header');
var jshint      = require('gulp-jshint');
var less        = require('gulp-less');
var notify      = require('gulp-notify');
var pkg         = require('./package.json');
var plumber     = require('gulp-plumber');
var prettify    = require('gulp-jsbeautifier');
var rename      = require('gulp-rename');
var sourcemaps  = require('gulp-sourcemaps');
var uglify      = require('gulp-uglify');

// -------------------------------------------------------------------------------------------------
// Shared resource path locations
// -------------------------------------------------------------------------------------------------
// jsWatchMin - Files that will be watched then minified into a single Javascript file upon changes
// jsLint - Files that will be ran through the linter
// cssBootstrap - File that will be used to compile LESS to CSS
// cssWatchMin - Files that will be watched for changes then compiled upon changes
// -------------------------------------------------------------------------------------------------
  var srcPaths = {
    jsWatchMin:   ['scripts/vendor/**/*.js', 'scripts/**/*.js'],
    jsLint:       ['scripts/**/*.js', '!scripts/vendor/**/*.js'],
    cssBootstrap: ['less/style.less'],
    cleanCSS:     ['less/**/*.less', '!styles/vendor/**/*.less'],
    cssWatchMin:  ['less/**/*.less']
  };

// -------------------------------------------------------------------------------------------------
// Build resource path locations
// -------------------------------------------------------------------------------------------------
// js - Compiled/minified Javascript output location
// css - Compiled/minified CSS output location
// -------------------------------------------------------------------------------------------------
  var buildPaths = {
    js: 'build/scripts',
    css: 'build/css'
  };

// -------------------------------------------------------------------------------------------------
// Error handler
// -------------------------------------------------------------------------------------------------
  var onError = function(err) {
    notify.onError({
      title:    'Gulp',
      subtitle: 'Failure!',
      message:  'Error: <%= error.message %>'
    })(err);
    this.emit('end');
  };

// -------------------------------------------------------------------------------------------------
// Header appended to all compiled files as a notice
// -------------------------------------------------------------------------------------------------
var compiledBanner = ['/**',
  ' * <%= pkg.name %> - <%= pkg.description %>',
  ' * @version <%= pkg.version %>',
  ' * @author <%= pkg.author %>',
  ' *',
  ' * COMPILED FILE DO NOT DIRECTLY EDIT',
  ' */',
  ''].join('\n');

// -------------------------------------------------------------------------------------------------
// JavaScript Tasks
// -------------------------------------------------------------------------------------------------

  // Runs JSLint and formats all custom JavaScript files except "vendor" files in the src/js/vendor directory.
  //
  // Configuration files:
  // JS Lint: /.jsintrc
  // JS Beautifer: /.jsbeautifyrc
  //
  // For more information view:
  // JS Lint: https://www.npmjs.org/package/gulp-jshint/
  // JS Beautifer: https://github.com/tarunc/gulp-jsbeautifier
  // -----------------------------
  gulp.task('cleanJS', function() {
    return gulp.src(srcPaths.jsLint, {base: './'})
      .pipe(plumber())
      .pipe(cache('cleanJS'))
      //.pipe(jshint())
      //.pipe(jshint.reporter('jshint-stylish'))
      .pipe(prettify({config: '.jsbeautifyrc'}))
      .pipe(gulp.dest('./'));
  });

  // Minifies all the JavaScript in the jsWatchMin paths
  // -----------------------------
  gulp.task('minifyJS', ['cleanJS'], function() {
    return gulp.src(srcPaths.jsWatchMin)
      .pipe(sourcemaps.init())
      .pipe(plumber())
      .pipe(concat('backbone.portfolio.min.js'))
      .pipe(uglify({
        mangle: true
      }))
      .pipe(header(compiledBanner, { pkg : pkg } ))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest(buildPaths.js));
  });

  // Build JavaScript tasks
  // -----------------------------
  gulp.task('buildJS', ['minifyJS']);

// -------------------------------------------------------------------------------------------------
// CSS Tasks
// -------------------------------------------------------------------------------------------------

  // Compile LESS files into CSS
  // -----------------------------
  gulp.task('compileCSS', function() {
    return gulp.src(srcPaths.cssBootstrap)
      .pipe(plumber({errorHandler: onError}))
      .pipe(sourcemaps.init())
      .pipe(less())
      .pipe(header(compiledBanner, { pkg : pkg } ))
      .pipe(cssnano())
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest(buildPaths.css))
      .pipe(notify({
        title: 'Gulp',
        subtitle: 'Success',
        message: 'LESS compiled'
      }));
  });

  // Runs CSSComb and formats all custom LESS files.
  //
  // Configuration files:
  // /.csscomb.json
  //
  // For more information view:
  // http://csscomb.com
  // -----------------------------
  gulp.task('cleanCSS', function() {
    return gulp.src(srcPaths.cleanCSS, {base: './'})
      .pipe(plumber({errorHandler: onError}))
      .pipe(cache('csscomb'))
      .pipe(csscomb())
      .pipe(gulp.dest('./'));
  });

  // Build CSS tasks
  // -----------------------------
  gulp.task('buildCSS', ['compileCSS']);

// -------------------------------------------------------------------------------------------------
// Watch Tasks
// -------------------------------------------------------------------------------------------------
  gulp.task('watch', function() {
    gulp.watch(srcPaths.jsWatchMin, ['buildJS']);
    gulp.watch(srcPaths.cssWatchMin, ['buildCSS']);
  });

// -------------------------------------------------------------------------------------------------
// The default task (called when you run `gulp` from cli)
// -------------------------------------------------------------------------------------------------
  gulp.task('default', ['buildJS', 'buildCSS', 'watch']);