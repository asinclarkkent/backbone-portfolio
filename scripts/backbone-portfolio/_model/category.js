// ===================
// Category model
// ===================
PortfolioApp.Models.Category = Backbone.Model.extend({
	defaults: {
		label: "Default",
		value: 'default',
		active: false
	}
});
