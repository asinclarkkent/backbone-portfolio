// ===================
// Portfolio Entry Model
// ===================
PortfolioApp.Models.Entry = Backbone.Model.extend({
	defaults: {
		title: "Default Project",
		subtitle: "A bit more about it",
		description: "Description lorem ipsum",
		img: "http://web.kentheberling.com/wp-content/themes/kentheberling/images/portfolio/sroc.jpg",
		categories: ["default"],
		active: true,
		link: 'http://kentheberling.com',
		linkText: 'View Project',
		editable: true
	}
});
