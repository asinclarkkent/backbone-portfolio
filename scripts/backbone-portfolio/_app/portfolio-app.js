// ===================
// Web Portfolio Backbone App
// ===================
var PortfolioApp = new(Backbone.View.extend({
	// MVC containers
	Models: {},
	Views: {},
	Collections: {},
	Values: {},
	Elements: {},
	init: function() {
		// Elements
		this.Elements.Title = jQuery("#add-title");
		this.Elements.Subtitle = jQuery("#add-subtitle");
		this.Elements.Description = jQuery("#add-description");
		this.Elements.Image = jQuery("#add-image");
		this.Elements.Link = jQuery("#add-link");
		this.Elements.Categories = jQuery("#add-categories");
		this.Elements.CategoryInput = jQuery("#add-category");

		this.Values.ScrollTo = jQuery('#category-selector-list').offset().top - 20;

		// Entries setup (collection and views)
		var entries = new this.Collections.EntriesLocal();
		entries.fetch();
		if (entries.models.length < 1) {
			entries.create({}); // add a starter!
		}
		this.Values.Entries = entries; // make it globally available

		// Entries view
		var entriesView = new this.Views.Entries({
			collection: entries
		});
		entriesView.render();
		this.Values.EntriesView = entriesView; // make it globally available
		jQuery('#portfolio-entries').html(entriesView.el);


		// Categories
		var categories = new this.Collections.CategoriesLocal(); // Categories used in the app
		categories.fetch();
		if (categories.models.length < 1) {
			categories.create({}); // add a starter!
		}
		this.Values.Categories = categories;

		// Categories view
		var categoriesView = new this.Views.Categories({
			collection: categories
		});
		categoriesView.render();
		this.Values.CategoriesView = categoriesView; // make it globally available
		jQuery('#category-selector-list').html(categoriesView.el);

		// Filtering on init!
		this.Values.EntriesView.filterByCategory();

		// Listeners
		this.listenTo(this.Values.Entries, 'add', this.renderNewEntry);
		this.listenTo(this.Values.Categories, 'add', this.renderNewCategory);
	},
	events: {
		"click #add-entry": "addEntry",
		"click #add-category-btn": "addCategory",
		"click #toggle-edit-mode": "toggleEditMode"
	},
	addEntry: function(e) {
		e.preventDefault();
		PortfolioApp.Values.Entries.create({
			title: this.Elements.Title.val(),
			subtitle: this.Elements.Subtitle.val(),
			description: this.Elements.Description.val(),
			img: this.Elements.Image.val(),
			link: this.Elements.Link.val(),
			categories: this.Elements.Categories.val().split(",")
		});
		this.clearEntryForm();
	},
	renderNewEntry: function(entry) {
		var view = new PortfolioApp.Views.Entry({
			model: entry
		});
		this.Values.EntriesView.showEntry(entry);
	},
	clearEntryForm: function() {
		this.Elements.Title.val('');
		this.Elements.Subtitle.val('');
		this.Elements.Description.val('');
		this.Elements.Image.val('');
		this.Elements.Link.val('');
		this.Elements.Categories.val('');
	},
	addCategory: function(e) {
		e.preventDefault();
		PortfolioApp.Values.Categories.create({
			label: this.Elements.CategoryInput.val(),
			value: this.Elements.CategoryInput.val().toLowerCase()
		});
		this.clearCategoryForm();
	},
	renderNewCategory: function(category) {
		// Need to render all so that the + icons in between are correct!
		PortfolioApp.Values.CategoriesView.render();
	},
	clearCategoryForm: function() {
		this.Elements.CategoryInput.val('');
	},
	toggleEditMode: function() {
		this.$el.toggleClass("view-mode");
		this.$el.find('span').toggle();
	}
}))({
	el: jQuery('#portfolio-app')
});
