// ===================
// Listing of Portfolio Entries
// ===================
PortfolioApp.Collections.EntriesLocal = Backbone.Collection.extend({
	model: PortfolioApp.Models.Entry,
	localStorage: new Backbone.LocalStorage("portfolio-entries"),
});
