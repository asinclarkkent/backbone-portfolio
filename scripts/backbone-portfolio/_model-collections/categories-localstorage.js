// ===================
// Listing of Portfolio Categories
// ===================
PortfolioApp.Collections.CategoriesLocal = Backbone.Collection.extend({
	model: PortfolioApp.Models.Category,
	localStorage: new Backbone.LocalStorage("portfolio-categories"),
});
