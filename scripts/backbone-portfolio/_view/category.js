// ===================
// View of a single category
// ===================
PortfolioApp.Views.Category = Backbone.View.extend({
	template: _.template('<div><a href="#" class="select <% if (active == true){ %>active<% } %>"><%= label %></a> <a class="delete  <% if (active == true){ %>disabled<% } %>"><i class="fa fa-trash"></i></a></div>'),
	model: PortfolioApp.Models.Category,
	render: function() {
		this.$el.html(this.template(this.model.attributes));
	},
	events: {
		'click a.select': 'categoryToggle',
		'click a.delete': 'clear'
	},
	initialize: function() {
		//this.listenTo(this.model, 'change', this.render);
		this.listenTo(this.model, 'destroy', this.remove);
	},
	categoryToggle: function(e) {
		e.preventDefault();

		// Find this category object
		var cat = this.model.get('value');

		// Toggle its active state
		this.model.set('active', !this.model.get('active'));
		// Re-render to reflect state!
		this.render();
		// Save it
		this.model.save({});

		// Filter the portfolio entries
		PortfolioApp.Values.EntriesView.filterByCategory();
	},
	clear: function(e) {
		if (!this.model.get("active")) {
			e.preventDefault();
			this.model.destroy();
			PortfolioApp.Values.CategoriesView.render();
			return false;
		}
	},
});
