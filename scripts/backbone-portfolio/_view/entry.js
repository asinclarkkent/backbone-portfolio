// ===================
// View of a single portfolio entry
// ===================
PortfolioApp.Views.Entry = Backbone.View.extend({
	template: _.template(
		'<div class="<% if (!active) { %>hideme <% } %>col-xs-12 col-md-6 portfolio-container <% for(var category in categories){ %><%= categories[category] %> <% } %>"><div class="portfolio-piece" ontouchstart="">' +
		'<img src="<%= img %>" />' +
		'<div class="entryText">' +
		'<h3><%= title %> <small>(<%= subtitle %>)</small></h3>' +
		'<p><%= description %></p>' +
		'<% if (link != "" && linkText != "") { %><p><a href="<%= link %>" target="_blank"><%= linkText %></a></p><% } %>' +
		'</div></div>' +
		'<% if (editable) { %><a href="#" class="edit"><fa class="fa fa-pencil"></i></a><% } %>' +
		'<% if (editable) { %><a href="#" class="delete"><fa class="fa fa-trash"></i></a><% } %>' +
		'<% if (editable) { %><a href="#" class="edit-done"><fa class="fa fa-close"></i></a><% } %>' +
		'<% if (editable) { %><a href="#" class="edit-save"><fa class="fa fa-save"></i></a><% } %>' +
		'<form class="entry-form">' +
		'<input class="add-title" placeholder="Title" value="<%= title %>"/>' +
		'<input class="add-subtitle" placeholder="Subtitle"  value="<%= subtitle %>"/>' +
		'<textarea class="add-description" placeholder="Description"><%= description %></textarea>' +
		'<input class="add-image" placeholder="Image link - 1000x560"  value="<%= img %>"/>' +
		'<input class="add-link" placeholder="Link"  value="<%= link %>"/>' +
		'<input class="add-categories" placeholder="Categories"  value="<% for(var category in categories){ %><%= categories[category] %>,<% } %>"/>' +
		'</form>' +
		'</div>'
	),
	model: PortfolioApp.Models.Entry,
	events: {
		'click .delete': 'clear',
		'click .edit': 'edit',
		'click .edit-done': 'editDone',
		'click .edit-save': 'editSave'
	},
	initialize: function() {
		this.listenTo(this.model, 'change', this.render);
		this.listenTo(this.model, 'destroy', this.remove);
	},
	render: function() {
		this.$el.html(this.template(this.model.attributes));

		// Inline editing inputs!
		this.inputTitle = this.$('.add-title');
		this.inputSubtitle = this.$('.add-subtitle');
		this.inputDescription = this.$('.add-description');
		this.inputImage = this.$('.add-image');
		this.inputLink = this.$('.add-link');
		this.categories = this.$('.add-categories');
	},
	clear: function(e) {
		e.preventDefault();
		this.model.destroy();
		return false;
	},
	edit: function(e) {
		e.preventDefault();
		this.$el.addClass("editing");
	},
	editDone: function(e) {
		e.preventDefault();
		this.$el.removeClass("editing");
	},
	cleanArray: function(array) {
		for (var i = 0; i < array.length; i++) {
			if (array[i] == '') {
				array.splice(i, 1);
				i--;
			}
		}
		return array;
	},
	editSave: function(e) {
		e.preventDefault();
		var categories = this.categories.val().split(',');
		this.model.save({
			title: this.inputTitle.val(),
			subtitle: this.inputSubtitle.val(),
			description: this.inputDescription.val(),
			img: this.inputImage.val(),
			link: this.inputLink.val(),
			categories: this.cleanArray(categories)
		});
		this.$el.removeClass("editing");
	}
});
