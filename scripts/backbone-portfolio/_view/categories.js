// ===================
// View of all Categories
// ===================
PortfolioApp.Views.Categories = Backbone.View.extend({
	initialize: function() {
		this.last = _.last(this.collection.models);
	},
	render: function() {
		this.showAll();
	},
	showAll: function() {
		this.$el.html(''); // clear HTML first
		this.last = _.last(this.collection.models); // update who is last
		this.collection.forEach(this.showCategory, this); // this will append to HTML
	},
	showCategory: function(category) {
		var thisCategory = new PortfolioApp.Views.Category({
			model: category
		});
		thisCategory.render();
		this.$el.append(thisCategory.el);

		if (category !== this.last) {
			this.$el.append('<i class="fa fa-plus"></i>');
		}
	}
});
