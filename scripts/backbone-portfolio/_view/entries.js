// ===================
// View of all Portfolio Entries
// ===================
PortfolioApp.Views.Entries = Backbone.View.extend({
	initialize: function() {
		//this.collection.on('add', this.showEntry, this);
		this.collection.on('reset', this.render, this);
		this.collection.on('filter', this.byTag, this);
	},
	render: function() {
		this.showAll();
	},
	showAll: function() {
		// clear HTML
		this.$el.html('');
		// Render collection!
		this.collection.forEach(this.showEntry, this);
	},
	showEntry: function(entry) {
		var thisEntry = new PortfolioApp.Views.Entry({
			model: entry
		});
		thisEntry.render();
		this.$el.append(thisEntry.el);
	},
	scrollView: function() {
		jQuery('html, body').animate({
			scrollTop: PortfolioApp.Values.ScrollTo
		}, 100);
	},
	filterByCategory: function() {
		var activeCatLength = 0,
			foundResult = false,
			activeCats = PortfolioApp.Values.Categories.where({
				active: true
			});
		activeCatLength = activeCats.length;

		this.collection.forEach(function(model) {
			// start back at 0
			var classCount = 0;

			// For each active class, see if this model has it
			for (var i = 0; i < activeCatLength; i++) {
				if (model.get("categories").indexOf(activeCats[i].attributes.value) != -1) {
					classCount++;
				}
			}

			// This is found in all selected categories
			if (classCount == activeCatLength) {
				model.set({
					"active": true
				});
				foundResult = true; // it only takes one
			} else {
				model.set({
					"active": false
				});
			}
		});

		if (foundResult) {
			this.showAll();
		} else {
			this.$el.html('<p class="text-center">Yeesh... nothing here. Drop a filter or two.</p>');
		}

		this.scrollView();
	}
});
